<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Chrisore') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <header role="heading">
        <div class="top-underlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="logo col-md-4 col-xs-12">
                    <img src="{{ 'images/chrisore-logo.png' }}" alt="Chrisore" />
                </div>
                </div>
            </div>

            <div class="row promo">
                <div class="col-md-10 col-md-offset-1">
                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                        <!-- Wrapper for slides -->
                        <div class="carousel-inner" role="listbox">
                            <div class="item active">
                                <img src="{{ 'images/header-1.png' }}" alt="..ubu.">
                            </div>
                            <div class="item">
                                <img src="{{ 'images/header-2.png' }}" alt=".ima..">
                            </div>
                            <div class="item">
                                <img src="{{ 'images/header-3.png' }}" alt="image">
                            </div>
                            <div class="item">
                                <img src="{{ 'images/header-4.png' }}" alt="image">
                            </div>
                        </div>
                    </div>
                </dvi>
            </div>
        </div>
    </header>

    <main id="app" role="main">
        @yield('content')
    </main>

    <footer role="contentinfo" class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1 footer">
                <ul>
                    <li><a href="{{ route('home') }}">Home</a></li>
                    <li><a href="{{ route('about') }}">Health and Safety</a></li>
                    <li><a href="#">Trainings</a></li>
                    <li><a href="#">Our Partners</a></li>
                </ul>
                <p>
                    <strong>&copy; {{ config('app.name', 'Chrisore') }} 2017. All right reserved. Designed by
                        <a href="http://loggcity.com.ng" style="color:#8C3D03;">Loggcity</a>
                    </strong>
                </p>
            </div>
        </div>
    </footer>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script type="text/javascript">
		$(document).ready(
			function(){
				$('.promo').innerfade({
					speed: 'slow',
					timeout: 4000,
					type: 'sequence',
					containerheight: '250px'
				});
		} );
    </script>
</body>
</html>
