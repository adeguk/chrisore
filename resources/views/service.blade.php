@extends('layouts.app')

@section('content')

<section id="midwrapper" role="article">
    <div class="container">
        <div class="row">
    	</div>
    </div>
</section>

<section role="contentinfo">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1" id="content">
                <div id="contact-front-left" class="col-xs-12 col-md-5">
                	<h1>Our Services</h1>
                    <p>An established leader in engineering services. Providing services in the construction industry in Nigeria, our engineering
                    excellence is available nationwide. Drawing on decades of experience, our team is dedicated to heritage of leading edge services
                    delivery and innovation as evident in all our projects executed in association with top flight consultants such as Architects,
                    Quantity Surveyors and Engineers over the years. </p>
                    <h2>Our activities in the industry include:</h2>
                    <dl>
                      <dt>1.	Plumbing Installation</dt>
                          <dd>&rArr;&nbsp;&nbsp;&nbsp;Industrial piping and Plants</dd>
                          <dd>&rArr;&nbsp;&nbsp;&nbsp;Water Main and Distributions</dd>
                          <dd>&rArr;&nbsp;&nbsp;&nbsp;Sprinklers and Fire Extinguisher Systems</dd>
                          <dd>&rArr;&nbsp;&nbsp;&nbsp;Swimming Pools and Water Fountains</dd>
                          <dd>&rArr;&nbsp;&nbsp;&nbsp;Swimming Pools and Water Fountains</dd>
                          <dd>&rArr;&nbsp;&nbsp;&nbsp;Bore Hole Drilling and Water Treatments</dd>
                          <dd>&rArr;&nbsp;&nbsp;&nbsp;Sewage Treatment Plant</dd>
                          <dd>&rArr;&nbsp;&nbsp;&nbsp;Fuel/Oil Storage and Printing Installation</dd>
                    </dl>
                </div>
                <div id="front-right" class="col-xs-12 col-md-7 xs-margin-bottom">
            <dl>
               <dt>2.	Air-Conditioning & Ventilation Installation</dt>
                  <dd>&rArr;&nbsp;&nbsp;&nbsp;The design, installation and commissioning of a ventilation and air-conditioning system for
                  residential, commercial and industrial application. Operations performed include cooling load estimates, dust sizing and
                  layout, determining fan power (static pressure) and coil capacities.</dd>
                  <dd>&rArr;&nbsp;&nbsp;&nbsp;The design and installation of an air quality management system using various air exchanges
                  mechanisms that will ensure adequate fresh air intake for proper ventilation and extract. These designs can be flexible
                  depending on considerations on site.</dd>
                  <dd>&rArr;&nbsp;&nbsp;&nbsp;The supply, fabrication, installation and commissioning of sheet metal air ducts in our new
                  state-of-the-art workshop, chilled water pipes and fitting along with its insulation, refrigerant pipes electrical
                  connections and their attendant controls and accessories.</dd>
                  <dd>&rArr;&nbsp;&nbsp;&nbsp;Troubleshooting and a fully backed comprehensive maintenance of the above systems, using a
                  reliable technical support crew with state-of-the-earth U.N. Complaint service tools to respond promptly to complain and
                  equipment failure, aided by our extensive communication network.</dd>
                  <dd>&nbsp;</dd>
               <dt>3.	Electrical Services</dt>
                  <dd>&rArr;&nbsp;&nbsp;&nbsp;Electrical Installation</dd>
                  <dd>&rArr;&nbsp;&nbsp;&nbsp;Fire Alarm System</dd>
                  <dd>&rArr;&nbsp;&nbsp;&nbsp;External Cabling</dd>
                  <dd>&rArr;&nbsp;&nbsp;&nbsp;Telecommunication and Workshop Equipment</dd>
                  <dd>&nbsp;</dd>
               <dt>4.	Supply and servicing of materials/equipment for building services</dt>
           </dl>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
