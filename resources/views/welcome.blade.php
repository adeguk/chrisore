@extends('layouts.app')

@section('content')

<section id="midwrapper" role="article">
    <div class="container">
        <div class="row">
    		<div class="col-md-10 col-md-offset-1">
                <div class="col-xs-12 col-md-5 xs-padding-bottom">
                    <h1>Welcome! Chrisore Engineering Limited</h1>
                    <p>A mechanical, Electrical and Air-Conditioning Engineering company incorporated in Nigeria via the
                        Corporate Affairs Commission, Abuja - Nigeria.</p>
                    <p>The directors, management and technical staff are dedicated, innovative and results oriented personnel
                        who have excelled in their various professional fields.</p>
                </div>
                <div class="col-xs-12 col-md-7 xs-margin-bottom">
                    <img src="{{ 'images/worldmap.png' }}" alt="" />
                </div>
            </div>
    	</div>
    </div>
</section>

<section role="contentinfo">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1" id="content">
        <div id="front-left_" class="col-xs-12 col-md-5">
        	<h2>Corporate Social Responsibility</h2>
            <p>To become one of the leading engineering service companies in Nigeria by delivering best quality
                services to our highly esteemed clients at all times. Commitments to developing and facilitating new
                capabilities in the industry for our clients nationwide through an interactive team approach
                encompassing our cross-functional team.</p>
            <p>To become one of the leading engineering service companies in Nigeria by delivering best quality
                services to our highly esteemed clients at all times.</p>
        </div>
        <div id="featured-service_" class="col-xs-12 col-md-3">
        	<h2>Featured Services</h2>
           	<ul>
                <li>Mechanical</li>
                <li>Electrical</li>
                <li>Instrumentation</li>
                <li>Water</li>
                <li>Air-Conditioning</li>
                <li>Security</li>
                <li>Industrial & Allied Services</li>
                <li>Construction</li>
            </ul>
        </div>
        <div id="front-right_" class="col-xs-12 col-md-4 xs-margin-bottom">
            <h2>Mission Statement</h2>
            <p>To become one of the leading engineering service companies in Nigeria by delivering best quality
                services to our highly esteemed clients at all times. Commitments to developing and facilitating
                new capabilities in the industry for our clients nationwide through an interactive team approach
                encompassing our cross-functional team.</p>
        </div>

            </div>
        </div>
    </div>
</section>
@endsection
