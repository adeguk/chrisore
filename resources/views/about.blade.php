@extends('layouts.app')

@section('content')

<section id="midwrapper" role="article">
    <div class="container">
        <div class="row">
    	</div>
    </div>
</section>

<section role="contentinfo">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1" id="content">
                <div id="front-left" class="col-xs-12 col-md-8">
                	<h1>Chrisore:: Who are we</h1>
                    <p>Chrisore Engineering Limited is a mechanical, Electrical and Air-Conditioning Engineering company incorporated in Nigeria
                    by the Corporate Affairs Commission, Abuja - Nigeria. The directors, management and technical staff are dedicated, innovative
                    and results oriented personnel who have excelled in their various professional fields.</p>
                    <p>Since inception, we have gained experience in delivering quality services to a wide range of discerning clients. We have,
                    over the years, built a considerable and vast experience in the industry with a team of Engineers, Technicians and supports
                    staff that are committed to providing products and services of the highest quality.</p>
                    <p>We firmly believe that unless a company is able to provide better products and services at competitive prices, it would not
                    be able to succeed in the ever-increasing competitive market in our environment. To ensure this, we have a continuous improvement
                     process that has resulted in a quality that sets new standards within the engineering service industry.</p>
                    <p>We consider our client's requirements and specifications, consult with leading materials and equipment manufacturers&frasl;
                    suppliers towards the promotion and development of innovative project delivery. </p>
                </div>
                <div id="front-right_" class="col-xs-12 col-md-4 xs-margin-bottom">
                    <h2>Vision Statement</h2>
                    <p>To provide the best possible service at competitive prices, while incorporating recent advances, and to provide the
                    technological service and exquisite craftsmanship.</p>
                    <h2>Mission Statement</h2>
                    <p>To become one of the leading engineering service companies in Nigeria by delivering best quality services to our highly
                     esteemed clients at all times. Commitments to developing and facilitating new capabilities in the industry for our clients
                     nationwide through an interactive team approach encompassing our cross-functional team.</p>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
