@extends('layouts.app')

@section('content')

<section id="midwrapper" role="article">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                @if(Session::has('message'))
                    <div class="alert alert-info">
                      {{Session::get('message')}}
                    </div>
                @endif
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
    	</div>
    </div>
</section>

<section role="contentinfo">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1" id="content">
                <div id="contact-front-left" class="col-xs-12 col-md-3" style="min-height:380px">
                	<h2>Corporate Head Office</h2>
                    <address>
                        <strong>Chrisore Engineering Limited</strong><br />
                        17 Udi Street<br />
                        Osborne Estate<br />
                        Ikoyi<br />Lagos.
                    </address>
                    <address>
                        <strong>Telephone:</strong> +234 (0) 1 7629257<br />
                    	<strong>Mobile:</strong> +234 (0) 803 325 7067<br />
                        <strong>Email:</strong> info@chrisore.com
                    </address>
                </div>
                <div id="featured-service_" class="col-xs-12 col-md-4" style="min-height:380px">
                	<h2>Send a request or comment</h2>

                    {!! Form::open(['route' => 'contact.store', 'id' => 'contactform']) !!}
                        <div class="form-group">
                            {{ Form::text('contactname', '', ['required', 'class' => 'form-control', 'placeholder'=>'Full Name']) }}
                        </div>
                        <div class="form-group">
                            {{ Form::text('email', '', ['required', 'class' => 'form-control', 'placeholder'=>'E-Mail Address']) }}
                        </div>
                        <div class="form-group">
                            {{ Form::text('tel', '', ['required', 'class' => 'form-control', 'placeholder'=>'Enter Telephone']) }}
                        </div>
                        <div class="form-group">
                            {{ Form::textarea('user_message', '', ['required', 'class' => 'form-control', 'placeholder'=>'Enter Message', 'rows'=>'3']) }}
                        </div>
                        {{ Form::submit('Send Message', ['class' => 'btn btn-primary']) }}
                    {!! Form::close() !!}
                </div>
                <div id="front-right" class="col-xs-12 col-md-4 xs-margin-bottom">
                    <p><strong>How to get here</strong></p>
                    <img src="{{ 'images/themap.png' }}" />
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
