@extends('layouts.email')

@section('content')

<section id="midwrapper" role="article">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <p>You received a new message from Chrisore.com:</p>
            </div>
    	</div>
    </div>
</section>

<section role="contentinfo">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1" id="content">
                <div id="contact-front-left" class="col-xs-12 col-md-3" style="min-height:300px">
                	<h2>Sender's Information</h2>
                    <strong>Full Name:</strong> {{ $contactname }}<br />
                    <strong>Email:</strong> {{ $email }}<br />
                    <strong>Mobile:</strong> {{ $tel }}<br />
                </div>
                <div id="featured-service_" class="col-xs-12 col-md-4" style="min-height:300px">
                	<h2>Message details or comment</h2>
                    <p>{{ $user_message }}</p>
                </div>
                <div id="front-right" class="col-xs-12 col-md-4 xs-margin-bottom">
                    <p><strong>Follow us:</strong></p>
                    Facebook<br />
                    Twitter<br />
                    Linkedin
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
