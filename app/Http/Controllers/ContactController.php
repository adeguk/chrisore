<?php

namespace App\Http\Controllers;

use Mail;
use App\Http\Requests\ContactFormRequest;

class ContactController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('contact');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ContactFormRequest $request) {
        // use Laravel Mail Driver to send email

        Mail::send('emails.contact',
            array(
                'contactname' => $request->get('contactname'),
                'email' => $request->get('email'),
                'tel' => $request->get('tel'),
                'user_message' => $request->get('user_message')
            ), function($mail)
        {
            $mail->from('do-not-reply@loggcity.co.uk');
            $mail->to('info@loggcity.co.uk', 'Customer Service')
                ->subject('Chrisore Feedback');
        });

        return \Redirect::route('contact')
            ->with('message', 'Thanks for contacting us!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
